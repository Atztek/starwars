import startmap from '~/components/svg/startmap'
import planetTemperate from '~/components/svg/planets/temperate'
import planetFrozen from '~/components/svg/planets/frozen'
import planetHot from '~/components/svg/planets/hot'
import planetWater from '~/components/svg/planets/water'
import planetArid from '~/components/svg/planets/arid'
import ship from '~/components/svg/ship'

import man from '~/components/svg/people/man'
import woman from '~/components/svg/people/woman'

import planetInfo from '~/components/planet-info'
import loader from '~/components/loader'

import Vue from 'vue'

Vue.component('startmap', startmap)
Vue.component('ship', ship)
Vue.component('planet-temperate', planetTemperate)
Vue.component('planet-frozen', planetFrozen)
Vue.component('planet-hot', planetHot)
Vue.component('planet-water', planetWater)
Vue.component('planet-arid', planetArid)

Vue.component('people-man', man)
Vue.component('people-woman', woman)

Vue.component('planet-info', planetInfo)
Vue.component('loader', loader)
