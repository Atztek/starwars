import * as axios from 'axios'

const instance = axios.create({
  baseURL: 'https://swapi.co/api/'
})

const target = {}
const handler = {
  get (target, name) {
    return Object.assign({},
      [
        'get',
        'delete',
        'options',
        'head'
      ].reduce(
        (o, method) => Object.assign({}, o, {
          [method] (url = '', params = {}) {
            if (typeof url === 'object') {
              params = url
              url = ''
            }
            if (url[url.length - 1] !== '/') {
              url += '/'
            }
            return instance[method](name + url, {
              params
            })
          }
        }), {}),
      [
        'post',
        'put',
        'patch'
      ].reduce(
        (o, method) => Object.assign({}, o, {
          [method] (url = '', body = {}, params = {}) {
            if (typeof url === 'object') {
              params = body
              body = url
              url = ''
            }

            if (url[url.length - 1] !== '/') {
              url += '/'
            }
            return instance[method](name + url, body, {
              params
            })
          }
        }), {})
    )
  }
}

export default new Proxy(target, handler)
