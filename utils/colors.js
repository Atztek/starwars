import csscolors from 'css-color-names'

csscolors['light'] = '#FFFFFF'
csscolors['fair'] = '#ffe0bd'
csscolors['unknown'] = '#FFFFFF'

export default csscolors
