import axios from '~/plugins/axios'

/* load all ships from api */
async function loadShips (loadurl) {
  loadurl = (typeof loadurl === 'undefined') ? '' : loadurl
  let {
    data
  } = await axios.starships.get(loadurl)
  let results = data.results

  if (data.next) {
    let query = url.parse(data.next, true).query
    let addsRes = await loadShips(query)
    results = _.concat(results, addsRes)
  }
  return results
}

async function loadPlanets(loadurl) {
  loadurl = (typeof loadurl === 'undefined') ? '' : loadurl
  let {
    data
  } = await axios.planets.get(loadurl)
  let results = data.results

  if (data.next) {
    let query = url.parse(data.next, true).query
    let addsRes = await loadPlanets(query);
    results = _.concat(results, addsRes)
  }
  return results
}

var getallships = new Promise(function (resolve, reject) {
  resolve(loadShips())
})

var getallplanets = new Promise(function(resolve, reject) {
  resolve(loadPlanets());
});

export { getallships, getallplanets };
