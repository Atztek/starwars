
import webpack from 'webpack'

module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: 'starter',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Global CSS
   */
  css: ['~/assets/css/main.scss'],
  /*
   ** Add axios globally
   */
  build: {
    plugins: [

      new webpack.ProvidePlugin({
        _: 'lodash',
        url: 'url',
        randomSeed: 'random-seed'
        // ...etc.
      })

    ],
    vendor: ['axios'],
    /*
     ** Run ESLINT on save
     */
    extend (config, { isDev }) {
      if (isDev && process.client) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  plugins: [
    '~/plugins/starmap',
    '~/plugins/vbar'
  ],
  serverMiddleware: [
    // API middleware
    '~/api/index.js'
  ]
}
