import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = () => new Vuex.Store({

  state: {
    ship: false,
    selectPlanet: false
  },
  getters: {
    ship: state => {
      return state.ship
    },
    selectPlanet: state => {
      return state.selectPlanet
    }
  },
  mutations: {
    selectShip: (state, ship) => {
      state.ship = ship
    },
    selectPlanet: (state, planet) => {
      state.selectPlanet = planet
    }
  }
})

export default store
